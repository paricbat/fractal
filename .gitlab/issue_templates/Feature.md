<!-- Please note that some features missing in the stable release are already available in the
development version. To avoid duplicates and unnecessary issues, please check that your request is
for something that is not yet implemented, and doesn’t have an existing issue that is open or that
was closed as out of scope. -->

Detailed description of the feature. Provide as much information as you can.

Proposed Mockups:

(Add mockups of the proposed feature)

## Design Tasks

* [ ]  design tasks

## Development Tasks

* [ ]  development tasks

## QA Tasks

* [ ]  quality assurance tasks
